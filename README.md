Mailer Laravel
========
This package contains a new mail driver that allows laravel to send emails using the default laravel methods.

##Getting Started
Install the application via compose
`composer require lshtmweb/mailer-laravel`.

Add to your service providers list.  
Register the mail service provider if you have not already done so.  
`$app->register(\Illuminate\Mail\MailServiceProvider::class);`  
Register the new mailer provider after the MailServiceProvider  
`$app->register(\Lshtmweb\MailerLaravel\MailerMailProvider::class);`  

Run the following to get the configuration file.
`php artisan vendor:publish`

Add your application credentials to you .env file.  
You will need to set up the following keys.

```
'MAILER_URL', 'http://mailer.app' //URL to your instance of mailer.
'MAILER_APP_ID', //Application ID generated when you register the application
'MAILER_APP_KEY' //Application key generated when you register application
```

##Sending an email
To send an email. You will first need to define certain parameters. There is a MailerObject class to help you do so.  
Construct your code as so to send using the built in mail wrapper.

```` 
$object = new MailerObject;
$object->setTemplate('templatekey');
$object->setRecipient("example@test.com");
SendsMailerEmails::sendEmail($object);
````

For more control, you can use the syntax below to get access to the full array of sending options.

```
$mailer = new MailerObject;
$mailer->setTemplate('templatekey');
$mailer->setRecipient("example@test.com");

Mail::raw('', function ($m) use ($mailer) {
         $m->to($mailer->getRecipient(), $mailer->getRecipient())->subject($mailer->getSubject());
         $m->getSwiftMessage()->mailerObject = $mailer;
});
```

You can also send the request using the MailerMail Facade

##Creating a Template
To create a template, you need to send a request.  
Create a data packet like this:
```
$data = [
    'title'        => 'Template title',
    'description'  => 'Template description',
    'content'      => 'Template content in HTML',
    'type'         => 1,
    'template_key' => 'TEMPLATE_KEY'
];
```
Please not that the template key is important to give you another way of calling the template without having to remember the template UUID.

Send the request using the MailerMail facade.

##Issues and Suggestions
If you find any issues, raise a [ticket!](https://bitbucket.org/lshtmweb/mailer-laravel/issues)