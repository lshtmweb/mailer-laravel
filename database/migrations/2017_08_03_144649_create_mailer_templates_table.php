<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMailerTemplatesTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('mailer_templates', function (Blueprint $table) {
                        $table->increments('id');
                        $table->string('key', 30);
                        $table->string('description')->nullable();
                        $table->string('extra_data', 24)->nullable();
                        $table->string('template_id', 48)->nullable();
                        $table->string('template_key')->nullable();
                        $table->timestamps();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::dropIfExists('mailer_templates');
        }
}
