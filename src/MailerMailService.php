<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 07/08/2017
 * Time: 16:30
 */

namespace Lshtmweb\MailerLaravel;


use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Builder;

class MailerMailService
{

        var $curl;
        /**
         * @var Collection
         */
        var $data;
        /**
         * @var string
         */
        var $url;
        var $app;

        /**
         * MailerMailService constructor.
         *
         * @param Application $app
         */
        public function __construct($app)
        {
                $this->app = $app;
                $this->curl = new Builder;
                $this->curl->withHeader('X-Requested-With: XMLHttpRequest')
                    ->asJsonResponse()
                    ->returnResponseObject();
                $this->url = $this->app['config']['mailer.url'];
                $this->data = new Collection;
                $this->initData();
        }

        public function initData()
        {
                if (!$this->data->isEmpty()) {
                        $this->data = new Collection;
                }
                $this->data->put('key', $this->app['config']['mailer.key']);
        }

        public function sendEmail($dataArray)
        {
                $this->data = $this->data->merge($dataArray);
                $curl = $this->curl->to($this->url . '/email');
                if (!empty($dataArray["attachments[0]"])) {
                        $curl->containsFile();
                }

                return $this->execute($curl, true);
        }

        /**
         * @param      $dataArray
         *
         * @param bool $save
         *
         * @return mixed
         */
        public function createTemplate($dataArray, $save = true)
        {
                $this->data = $this->data->merge($dataArray);
                $curl = $this->curl->to($this->url . '/templates');

                $template = $this->execute($curl, true);
                if ($save) {
                        $mailerTemplate = new MailerMailTemplate;
                        $mailerTemplate->template_key = $template->results->key;
                        $mailerTemplate->key = $dataArray['template_key'];
                        $mailerTemplate->template_id = $template->results->reference;
                        $mailerTemplate->description = $dataArray['description'];
                        try {
                                $mailerTemplate->save();
                        } catch ( \Exception $e ) {
                                Log::warning("Problem saving the template because {$e->getMessage()}. You will have to do it manually.");
                        }
                }

                return $template;
        }

        /**
         * @param Builder $curl
         * @param bool    $post
         *
         * @return mixed
         * @throws MailerMailException
         */
        public function execute($curl, $post = false)
        {
                $curl->withData($this->data->toArray());
                if ($post) {
                        $response = $curl->post();
                } else {
                        $response = $curl->get();
                }
                $this->initData();
                if ($response->status != Response::HTTP_OK) {
                        throw new MailerMailException;
                } else {
                        return $response->content;
                }
        }
}