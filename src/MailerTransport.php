<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 20/07/2017
 * Time: 12:20
 */

namespace Lshtmweb\MailerLaravel;


use Illuminate\Mail\Transport\Transport;
use Illuminate\Support\Facades\Log;
use Swift_Mime_Message;

class MailerTransport extends Transport
{
        var $mailerService;

        /**
         * MailerTransport constructor.
         *
         * @param MailerMailService $mailerService
         * @param array             $configuration array
         */
        public function __construct(MailerMailService $mailerService, $configuration)
        {
                $this->key = $configuration['key'];
                $this->url = $configuration['url'];
                $this->id = $configuration['id'];
                $this->mailerService = $mailerService;
        }

        /**
         * Send the given Message.
         *
         * Recipient/sender data will be retrieved from the Message API.
         * The return value is the number of recipients who were accepted for delivery.
         *
         * @param Swift_Mime_Message $message
         * @param string[]           $failedRecipients An array of failures by-reference
         *
         * @return int
         */
        public function send(Swift_Mime_Message $message, &$failedRecipients = null)
        {

                /** @var MailerMailObject $mailerObject */
                $mailerObject = $message->mailerObject;
                $dataArray = [
                    "recipient" => array_first($message->getTo()),
                    "cc"        => array_first($message->getCc()),
                    "bcc"       => array_first($message->getBcc()),
                    "from"      => !empty($message->getFrom()) ? array_first($message->getFrom()) : $message->getSender(),
                    "template"  => $mailerObject->getTemplate(),
                    "params"    => $mailerObject::prepareParams($mailerObject->getParams()->toArray())
                ];

                if (!empty($mailerObject->getDelimiter()))
                {
                        $dataArray["delimiter"] = $mailerObject->getDelimiter();
                }
                if (!empty($mailerObject->getEscapeChar()))
                {
                        $dataArray["escape_char"] = $mailerObject->getEscapeChar();
                }

                if (!$mailerObject->getAttachments()->isEmpty()) {
                        $mailerObject->getAttachments()->each(function ($attachment, $key) use (&$dataArray) {
                                $realpath = realpath($attachment);
                                $file_info = pathinfo($realpath);
                                $dataArray["attachments[{$key}]"] = new \CURLFile($realpath, mime_content_type($realpath), "mailer_attach_{$key}.{$file_info["extension"]}");
                        });
                }
                $this->beforeSendPerformed($message);
                try {
                        $response = $this->mailerService->sendEmail($dataArray);
                        Log::info("Email scheduled successfully via {$response->results->sent_via}");

                } catch ( MailerMailException $e ) {
                        Log::error("Something went wrong queuing emails. Status is {$e->getMessage()}");
                } finally {
                        return $this->numberOfRecipients($message);
                }
        }
}